<html>
	<head>
		<title>PHP Test</title>
	</head>
	<body>
		<?php
			echo __DIR__;
			require('/home/vhosts/richard.xp3.biz/test.php');
			require('/home/vhosts/richard.xp3.biz/vendor/paypal/sdk-core-php-stable-php5.3/tests/PPBootStrap.php');
			require('/home/vhosts/richard.xp3.biz/vendor/paypal/merchant-sdk-php-stable-php5.3/samples/PPBootStrap.php');
			$config = array (
			'mode' => 'sandbox' , 
			'acct1.UserName' => 'jb-us-seller_api1.paypal.com',
			'acct1.Password' => 'WX4WTU3S8MY44S7F', 
			'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy'
			);
			$paypalService = new \PayPal\Service\PayPalAPIInterfaceServiceService($config);
			$paymentDetails= new \PayPal\EBLBaseComponents\PaymentDetailsType();
			
			$itemDetails = new \PayPal\EBLBaseComponents\PaymentDetailsItemType();
			$itemDetails->Name = 'item';
			$itemAmount = '1.00';
			$itemDetails->Amount = $itemAmount;
			$itemQuantity = '1';
			$itemDetails->Quantity = $itemQuantity;
			
			$paymentDetails->PaymentDetailsItem[0] = $itemDetails;
			
			$orderTotal = new \PayPal\CoreComponentTypes\BasicAmountType();
			$orderTotal->currencyID = 'USD';
			$orderTotal->value = $itemAmount * $itemQuantity; 
			
			$paymentDetails->OrderTotal = $orderTotal;
			$paymentDetails->PaymentAction = 'Sale';
			
			$setECReqDetails = new \PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType();
			$setECReqDetails->PaymentDetails[0] = $paymentDetails;
			$setECReqDetails->CancelURL = 'http://richard.xp3.biz/cancelled.php';
			$setECReqDetails->ReturnURL = 'http://richard.xp3.biz/sumup.php';
			
			$setECReqType = new \PayPal\PayPalAPI\SetExpressCheckoutRequestType();
			$setECReqType->Version = '104.0';
			$setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
			
			$setECReq = new \PayPal\PayPalAPI\SetExpressCheckoutReq();
			$setECReq->SetExpressCheckoutRequest = $setECReqType;
			
			$setECResponse = $paypalService->SetExpressCheckout($setECReq);
			var_dump($setECResponse);
			$token = $setECResponse->Token;
			$url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";
			$url = $url . $token;
			echo $url;
			header('Location: '.$url);
		?>
	</body>
</html>
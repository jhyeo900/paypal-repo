########Introduction########
Please be noted that it is my first time doing web developing, not to mention PHP, javascript, etc.
While not so graceful, the project shows you how you can use PayPal Express Checkout to make easy payments.
The project is at basic level with minimal functionalities.

########mainpage.php########
This is the page where visitors should be directed to on their first visit.

It is very minimal with no options to do anything other than
1) make payment
2) pop up on the right side to tell visitors about paypal

Upon clicking on PayPal Express Checkout button, the vistor will be redirected to PayPal sandbox.

########cancelled.php########
When visitor cancels transactions at PayPal payment page, the visitor will be directed to this page,
with a one-liner saying that no payment has been made and an option to go back to mainpage.php

########success.php########
When the payment is successful, the visitor is directed to this page, which has similar format compared
to the cancelled.php

########failed.php########
When the payment is not successful for some reason, the visitor is directed to this page. Currently, there
is no additional message to inform visitors of the reason for the failure. Again, this page has similar format
compared to cancelled.php

########payment.php########
This is the component that is responsible for initating Express Checkout as SetExpressCheckout call is initiated
here.

########sumup.php########
This is the component that the visitor is returned to upon successful interactions with PayPal Express Checkout service
PayerID and token from PayPal is extracted from URL before committing the transaction. It only checks whether the 
transaction has been successful from the response given by PayPal before directing the visitor to either success.php
or failed.php
